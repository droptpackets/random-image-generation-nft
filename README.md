# Random Image Generation NFT

### License:
MIT

## FAQ:
### How do I run this?
To run the entire traits and image generation at once, run the following from the project folder:

`python generate_nft_metadata_and_images.py`

### What about my metadata? Why is it in two places?
Metadata will generate in two forms: 

First, in the same format Boring Bananas Co originally used, in a traits_only.json file.

Second, the script will also automatically create IPFS-ready data in the metadata_format folder. This data is formatted for Open Sea, the only thing left to add will be the image IPFS CIDs after generation.

### Update: I've added a single file to organize all types including randomness weights!
Please update and add to the base_traits_data.json file. Extend it as far as you like, but make sure randomness weight total never exceeds 100.
