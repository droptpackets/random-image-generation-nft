from PIL import Image
import json
from list_types import get_dict_types_with_filepath
from concurrent.futures import ProcessPoolExecutor
import os

# All files defined here MUST exist in the image_generation/image_data/<FILETYPE HERE>/<FILENAME HERE> in
# order to generate
# Example: Background file for "White" below corresponds to bg001, which would
# be located at: $PROJECT_DIR/image_generation/image_data/backgrounds/bg001.png
#
# Please make sure to have all of your images properly named and in place before running the script
# or you will get errors

with open('base_traits_data.json') as f:
    base_traits = json.load(f)

backgroundfiles = get_dict_types_with_filepath(base_traits['backgrounds'])
bodyfiles = get_dict_types_with_filepath(base_traits['bodies'])
mouthfiles = get_dict_types_with_filepath(base_traits['mouths'])
eyefiles = get_dict_types_with_filepath(base_traits['eyes'])
heads = get_dict_types_with_filepath(base_traits['heads'])
head_backs = get_dict_types_with_filepath(base_traits['heads_back'])
accessories = get_dict_types_with_filepath(base_traits['accessories'])
accessories_backs = get_dict_types_with_filepath(base_traits['accessories_backs'])


def generate_images(traits):
    if not os.path.exists('./output/'):
        os.makedirs('./output/')

    for trait in traits:
        create_image(trait)

    # with ProcessPoolExecutor() as executor:  # Commented this out as it suppresses errors. Need to refactor.
    #     executor.map(create_image, traits)


def create_image(nft_image):
    im1 = Image.open(f'./image_data/backgrounds/{backgroundfiles[nft_image["Background"]]}.png').convert('RGBA')
    im2 = Image.open(f'./image_data/bodies/{bodyfiles[nft_image["Body"]]}.png').convert('RGBA')
    im3 = Image.open(f'./image_data/mouths/{mouthfiles[nft_image["Mouth"]]}.png').convert('RGBA')
    im4 = Image.open(f'./image_data/eyes/{eyefiles[nft_image["Eyes"]]}.png').convert('RGBA')
    im5 = Image.open(f'./image_data/heads/{heads[nft_image["Headwear"]]}.png').convert('RGBA')
    im_h_bg = Image.open(f'./image_data/heads/{head_backs[nft_image["Headwear"]]}.png').convert('RGBA')
    im_ac = Image.open(f'./image_data/accessory/{accessories[nft_image["Accessories"]]}.png').convert('RGBA')
    im_ac_bg = Image.open(f'./image_data/accessory/{accessories_backs[nft_image["Accessories"]]}.png').convert('RGBA')

    print(f'Loaded images for Id: {nft_image["tokenId"]}')

    com0 = Image.alpha_composite(im_ac_bg, im_h_bg)
    com1 = Image.alpha_composite(com0, im2)
    com2 = Image.alpha_composite(com1, im3)
    com3 = Image.alpha_composite(com2, im4)
    com4 = Image.alpha_composite(com3, im_ac)
    com5 = Image.alpha_composite(com4, im5)

    rgb_im = com5.convert('RGBA')
    rgb_im = rgb_im.resize((3000, 3000))
    file_name_character_only = str(nft_image["tokenId"]) + "_character_only.png"
    if not os.path.exists('./output/character_only/'):
        os.makedirs('./output/character_only/')
    rgb_im.save("./output/character_only/" + file_name_character_only)
    # Generates character only in case you want to use it for marketing or other graphics use

    com_final = Image.alpha_composite(im1, com5)

    rgb_im = com_final.convert('RGB')
    rgb_im = rgb_im.resize((3000, 3000))

    file_name = str(nft_image["tokenId"]) + ".jpg"
    rgb_im.save("./output/" + file_name)  # Saves full size render of image for NFT

    rgb_im_thumb = rgb_im.resize((600, 600))
    file_name = str(nft_image["tokenId"]) + ".jpg"
    if not os.path.exists('./output/thumbnails/'):
        os.makedirs('./output/thumbnails/')
    rgb_im_thumb.save("./output/thumbnails/" + file_name)  # Saves lower resolution render of image for NFT

    print(f'Image created for Id: {nft_image["tokenId"]}')  # Keeps track of where we are. As we are using a
    #                                                       # processPoolExecutor, it is expected that the image created
    #                                                       # will be out of order during generation process (ie,
    #                                                       # creates #3 ahead of #1, etc).

