import traits_generation
import image_generation

if __name__ == "__main__":
    traits = traits_generation.generate_and_write_nft_format()  # Creates your random metadata
    image_generation.generate_images(traits)  # Passes your metadata to generation logic to create images
