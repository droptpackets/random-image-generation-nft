
def list_all_types(i, type_to_return, trait_name):
    types = []
    for x in i:
        types.append(x[type_to_return])
    if type_to_return == 'weight':
        check = 0
        for t in types:
            check += t
        try:
            assert(check == 100)
        except Exception as e:
            print(f'Weights do not add up to 100, instead total is: {check}. '
                  f'Check that >> {trait_name} << weights are correct.')
            exit()
    return types


def get_dict_types_with_filepath(i):
    paths = {}
    for x in i:
        paths[x['type']] = x['file']
    return paths
