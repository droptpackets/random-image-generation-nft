import random
import json
from list_types import list_all_types

TOTAL_NFT_Characters = 10  # Number of images you want to generate - should be larger number, limited to 10 for example

traits = []


def create_combo(backgrounds, backgroundweights, bodies, bodyweights, mouths, mouthweights, eyes, eyesweights, head, headweights, accessories, accessoryweights):
    trait = {"Background": random.choices(backgrounds, backgroundweights)[0],
             "Body": random.choices(bodies, bodyweights)[0], "Mouth": random.choices(mouths, mouthweights)[0],
             "Eyes": random.choices(eyes, eyesweights)[0], "Headwear": random.choices(head, headweights)[0],
             "Accessories": random.choices(accessories, accessoryweights)[0]}

    if trait in traits:
        #         if this NFT_Character was already in the Traits list, restart the function
        return create_combo(backgrounds, backgroundweights, bodies, bodyweights, mouths, mouthweights, eyes, eyesweights, head, headweights, accessories, accessoryweights)
    else:
        return trait


# ARE ALL NFT_CharacterS UNIQUE?
def all_unique(x):
    seen = list()
    return not any(i in seen or seen.append(i) for i in x)


def format_for_nft_metadata(generated_traits):
    for trait in generated_traits:
        trait_types = ["Background", "Body", "Eyes", "Mouth", "Headwear", "Accessories"]
        trait_list = []
        for trait_type in trait_types:
            trait_list.append({"trait_type": f"{trait_type}", "value": f"{trait[trait_type]}"})

        ipfs_ready_trait = {"image": f"replaceTextHereWithIPFSResourceCID/{trait['tokenId']}.jpg",
                            "tokenId": f"{trait['tokenId']}",
                            "name": f"NFT_Characters #{trait['tokenId']}",
                            "attributes": trait_list
                            }
        with open(f'../traits/metadata_format/{trait["tokenId"]}', 'w') as outfile:
            json.dump(ipfs_ready_trait, outfile, indent=4)


def generate_all_traits():
    with open('base_traits_data.json') as f:
        base_traits = json.load(f)

    backgrounds = list_all_types(base_traits['backgrounds'], 'type', 'backgrounds')
    backgroundweights = list_all_types(base_traits['backgrounds'], 'weight', 'backgrounds')
    bodies = list_all_types(base_traits['bodies'], 'type', 'bodies')  # Should have at least 10 per category
    bodyweights = list_all_types(base_traits['bodies'], 'weight', 'bodies')
    mouths = list_all_types(base_traits['mouths'], 'type', 'mouths')
    mouthweights = list_all_types(base_traits['mouths'], 'weight', 'mouths')
    eyes = list_all_types(base_traits['eyes'], 'type', 'eyes')
    eyesweights = list_all_types(base_traits['eyes'], 'weight', 'eyes')
    head = list_all_types(base_traits['heads'], 'type', 'heads')
    headweights = list_all_types(base_traits['heads'], 'weight', 'heads')
    accessories = list_all_types(base_traits['accessories'], 'type', 'accessories')
    accessoryweights = list_all_types(base_traits['accessories'], 'weight', 'accessories')

    for i in range(TOTAL_NFT_Characters):
        newtraitcombo = create_combo(backgrounds, backgroundweights, bodies, bodyweights, mouths, mouthweights, eyes, eyesweights, head, headweights, accessories, accessoryweights)
        traits.append(newtraitcombo)


    # ADD TOKEN IDS TO JSON
    i = 0
    for item in traits:
        item["tokenId"] = i
        i = i + 1

    # GET TRAIT COUNTS

    background_counts = {}
    for item in backgrounds:
        background_counts[item] = 0

    body_counts = {}
    for item in bodies:
        body_counts[item] = 0

    mouth_counts = {}
    for item in mouths:
        mouth_counts[item] = 0

    eyes_counts = {}
    for item in eyes:
        eyes_counts[item] = 0

    head_counts = {}
    for item in head:
        head_counts[item] = 0

    acc_counts = {}
    for item in accessories:
        acc_counts[item] = 0

    for nFT_Character in traits:
        background_counts[nFT_Character["Background"]] += 1
        body_counts[nFT_Character["Body"]] += 1
        mouth_counts[nFT_Character["Mouth"]] += 1
        eyes_counts[nFT_Character["Eyes"]] += 1
        head_counts[nFT_Character["Headwear"]] += 1
        acc_counts[nFT_Character["Accessories"]] += 1

    #print(background_counts)
    #print(body_counts)
    #print(mouth_counts)
    #print(eyes_counts)
    #print(head_counts)
    #print(acc_counts)

    with open('../traits/traits_only.json', 'w') as outfile:
        json.dump(traits, outfile, indent=4)

    return traits


def generate_and_write_nft_format():
    generated_traits = generate_all_traits()
    format_for_nft_metadata(generated_traits)
    return generated_traits


if __name__ == "__main__":
    format_for_nft_metadata(generate_all_traits())
